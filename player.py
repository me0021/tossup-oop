class Player():

    points = 0
    tempPoints = 0

    def __init__(self):
        return

    def getPoints(self):
        return self.points

    def addTempPointsToTotal(self, numTempPoints):
        self.tempPoints += numTempPoints

    def getTempTotal(self):
        return self.tempPoints + self.points

    def updatePointsWithTemp(self):
        self.points += self.tempPoints

    def resetTemp(self):
        self.tempPoints = 0

    def getTempScore(self):
        return self.tempPoints

    def setTempPoints(self, points):
        self.tempPoints = points