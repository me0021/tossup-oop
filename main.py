from controller import *

def main():
    # init controller with number of
    c = Controller()
    c.setNumDice(10)
    c.setNumPlayers(2)
    c.play()
    exit(1)
    return



if __name__ == '__main__':
    main()