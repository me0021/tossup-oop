import random

class Dice:

    numDice = 0
    die = ['green', 'yellow', 'green', 'yellow', 'red', 'green']
    result = {"green": 0, "red": 0, "yellow": 0}

    # constructor
    def __init__(self, numDice):
        self.numDice = 10
        return

    # Generate Rnadom Integer in range
    def generate_random(self, max_range):
        return random.randint(0, max_range)

    def setNumDice(self, numDice):
        self.numDice = numDice

    def getNumDice(self):
        return self.numDice

    def rollDice(self):
        # generate random rolls between 0 and 5 (1 and 6)
        self.result = {"green": 0, "red": 0, "yellow": 0}
        for x in range(self.numDice):
            face = self.die[self.generate_random(5)]
            # add 1 to each face if rolled
            self.result[face] += 1
        self.printRoll()

    def getg(self):
        return self.result["green"]

    def gety(self):
        return self.result["yellow"]

    def getr(self):
        return self.result["red"]

    def returnResult(self):
        return self.result

    def printRoll(self):
        print("\n You rolled " + str(self.result['green'])
              + " greens, " + str(self.result['yellow'])
              + " yellows, and " + str(self.result['red'])
              + " reds.\n")