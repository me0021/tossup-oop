from dice import *
from player import *

class Controller():

    players = []
    curPlayerIdx = 0
    invalidFlag = 0
    lastRoundFlag = 0
    firstRoundFlag = 1

    def __init__(self):
        return

    def play(self):
        self.tossUpPrintIntroduction()
        while not self.lastRoundFlag:
            # switch the players
            if self.curPlayerIdx < (len(self.players) - 1) and not self.firstRoundFlag:
                self.firstRoundFlag = 0
                self.curPlayerIdx += 1
            else:
                self.curPlayerIdx = 0
                self.firstRoundFlag = 0
            self.printPlayer()
            self.dice.rollDice()
            self.processDiceRoll()
            temp = 0
            for player in self.players:
                temp += 1
                if player.getPoints() > 100:
                    self.printLastRoundRules(temp)
            # check if any player is at the limit
            # if they are, handle last round
        return

    def processDiceRoll(self):
        if self.dice.getg():
            self.makeChoice()
        elif self.dice.getr():
            print("\nWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPS\n")
            print("---->>>> Whoops! You don't gain points for this round.")
            print("\nWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPSWHOOPS\n")
            self.players[self.curPlayerIdx].resetTemp()
            self.dice.setNumDice(10)
        elif self.dice.gety():
            self.makeChoice()

    def makeChoice(self):
        if not self.dice.gety() and not self.dice.getr():
            self.dice.setNumDice(10)
        else:
            self.dice.setNumDice(self.dice.getr() + self.dice.gety())
        choice = input("\n---->>>> Add "
                       + str(self.players[self.curPlayerIdx].getTempScore() + self.dice.getg())
                       + " to Player "
                       + str(self.curPlayerIdx + 1)
                       + "'s score for a total of "
                       + str(self.players[self.curPlayerIdx].getTempTotal() + self.dice.getg())
                       + " points? Press K."
                         "\n---->>>> Roll "
                       + str(self.dice.getNumDice())
                       + " dice? Press R.\n"
                       "(To quit, press Q.)\n").lower()
        if choice in ("R", "r"):  # if they want to roll again
            self.players[self.curPlayerIdx].addTempPointsToTotal(self.dice.getg())
            if self.dice.numDice is 0:  # if the number of dice has rolled over
                self.dice.setNumDice(10) # set it back to the full number
            self.dice.rollDice()
            self.processDiceRoll()
        elif choice in ("K", "k"):  # if they don't want to roll again
            self.players[self.curPlayerIdx].addTempPointsToTotal(
                self.dice.getg()
            )
            self.players[self.curPlayerIdx].updatePointsWithTemp()
            self.players[self.curPlayerIdx].setTempPoints(0)
            self.dice.setNumDice(10)
            for player in self.players:
                if player.getPoints() > 100:
                    self.printLastRoundRules(self.curPlayerIdx)
            if self.lastRoundFlag:
                self.printFinalScore()
            return
        elif choice in ("Q", "q"):
            exit(1)
        else:
            print("Please input to roll again or keep.\n")
            self.makeChoice()
        return

    # Set the number of dice from Main
    def setNumDice(self, inputDice):
        self.dice = Dice(inputDice)

    # Set the number of players from Main
    def setNumPlayers(self, inputPlayerNum):
        for x in range(inputPlayerNum):
            y = Player()
            self.players.append(y)

    # Print introductory statement
    def tossUpPrintIntroduction(self):
        # print introduction
        print("\nBe the player with the highest "
              "score over 100 at the end of the game. \n"
              "\n")
        # give option to print longer introduction.
        choice = input("Press B to begin or H for rules.\n"
                       "Press any key to begin. \n").lower()
        # process input
        if choice in ("H", "h"):
            self.printRules()
        return

    def printScores(self):
        x = 0
        print("\n")
        for player in self.players:
            print("Player " + str(x+1) + " has " + str(player.getPoints()) + " points!\n")
            x += 1

    def printLastRoundRules(self, temp):
        self.curPlayerIdx = temp
        print("\nLASTROUNDLASTROUNDLASTROUNDLASTROUNDLASTROUNDLASTROUND\n")
        print("Player " + str(temp) + " is over 100 points!\n"
                                        "Each player gets one more turn to "
                                        "try and get more points than Player " + str(temp) + ".")
        print("\nLASTROUNDLASTROUNDLASTROUNDLASTROUNDLASTROUNDLASTROUND\n")
        # set last round flag
        self.lastRoundFlag = 1
        # change plyers
        if self.curPlayerIdx is 0:
            self.curPlayerIdx = 1
        else:
            self.curPlayerIdx = 0
        # set new dice roll
        self.dice.setNumDice(10)
        self.dice.rollDice()

    # print the introductory rules
    def printRules(self):
        print("Decide who goes first. On your turn, \n"
              "type \'roll\' to roll all your dice. \n"
              "\n"
              "If there are any greens, you may choose to \n"
              "set them aside and roll the remaining dice.\n"
              "If all 10 dice are eventually green, you \n"
              "may roll again.\n"
              "\n"
              "If on any roll you don't show any greens\n"
              "and you get one or more reds, your turn \n"
              "ends and you don't get any points for \n"
              "any greens gained in that round!\n"
              "\n"
              "If you roll all yellows you can roll \n"
              "again or end your turn and take your\n"
              "points.\n"
              "\n"
              "When one player has over 100 points at \n"
              "the end of their turn, all players get \n"
              "one last chance to beat the other player's\n"
              "score. Then, the game ends.\n")
        return

    # change players
    def printPlayer(self):
        print("\n---------------------------------------")
        print("\nIt's Player " + str(self.curPlayerIdx + 1) + "'s turn!")

    def printFinalScore(self):
        winner = 0
        temp = 0
        for player in self.players:
            if player.getPoints() > winner:
                winner = player.getPoints()
                currentWinner = temp
            elif player.getPoints() < winner:
                temp += 1
            else:
                print("It's a tie! Final Score: ")
                self.printScores()
                exit(1)
        print("\nWINNERWINNERWINNERWINNERWINNERWINNERWINNERWINNERWINNERWINNER\n\n"
              "Congratulations, Player "
              + str(currentWinner) + "! "
                                     "Final Score: \n")
        self.printScores()
        print("\nWINNERWINNERWINNERWINNERWINNERWINNERWINNERWINNERWINNERWINNER\n")
        exit(1)

